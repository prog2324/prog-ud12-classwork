package es.cipfpbatoi.controllers;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.cipfpbatoi.model.Tarea;
import es.cipfpbatoi.services.MariadbConnection;

@Controller
public class Act04Controller {

	@GetMapping("/tarea-default-add2")
	@ResponseBody
	public String insertarTareaPorDefecto(@RequestParam String desc, @RequestParam String usr) {
		StringBuilder response = new StringBuilder();
		String sql = "insert into tareas values (null, ?, ?, ?, ?, ?, ?, null);";
		
		try(PreparedStatement st = MariadbConnection.getConnection().prepareStatement(sql)) {
			Tarea tarea = new Tarea(desc, usr);
			st.setString(1, tarea.getUsuario());
			st.setString(2, tarea.getDescripcion());
			st.setTimestamp(3, Timestamp.valueOf(tarea.getFechaCreacion()));
			st.setTimestamp(4, Timestamp.valueOf(tarea.getFechaVencimiento()));
			st.setString(5, tarea.getPrioridad().toString());
			st.setBoolean(6, tarea.isRealizada());
			// st.setNull(7, Types.INTEGER);
			
			System.out.println(sql);
			int filasAfectadas = st.executeUpdate();		
			
			response.append("<p style='color:blue'><strong>Inserción de tarea </strong>: realizada correctamente​</p>");
			response.append("Filas afectadas: " + filasAfectadas);
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return "<h1> Algo ha ido mal... </h1>";
		}
			
		
		return response.toString();
	}
}
