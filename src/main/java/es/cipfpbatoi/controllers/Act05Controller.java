package es.cipfpbatoi.controllers;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.cipfpbatoi.services.MariadbConnection;

@Controller
public class Act05Controller {
	
	@GetMapping("/tarea-description-update")
	@ResponseBody
	public String insertaTareaPorDefecto(@RequestParam String codigo, @RequestParam String descripcion) {
		StringBuilder response = new StringBuilder();
		String sql = "update tareas set descripcion = ? where codigo = ?";
		try (PreparedStatement statement = MariadbConnection.getConnection().prepareStatement(sql);){
			
			statement.setString(1, descripcion);
			statement.setInt(2, Integer.parseInt(codigo));
			
//			System.out.println(statement.toString());
			int filasAfectadas = statement.executeUpdate();
			
			response.append("<p style='color:blue'><strong>Modificación de tarea </strong>: realizada correctamente​</p>");
			response.append("Filas afectadas: " + filasAfectadas);
			
			return response.toString();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return "<h1> Algo ha ido mal... </h1>";
		}		
	}
}
