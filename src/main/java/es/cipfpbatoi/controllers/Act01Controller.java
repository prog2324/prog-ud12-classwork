package es.cipfpbatoi.controllers;

import java.sql.Connection;
import java.sql.SQLException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.cipfpbatoi.services.MariadbConnection;

@Controller
public class Act01Controller {
	
	@GetMapping("/testconectabd")
	@ResponseBody
	public String testConectaBD() {		
		try {
			Connection con = MariadbConnection.getConnection();
			return "<h1> Conexión exitosa: </h1>" + con;
		} catch (SQLException e) {
			return "<h1> Conexión fallida </h1>";
		}		
	}
}
