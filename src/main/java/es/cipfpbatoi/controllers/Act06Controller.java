package es.cipfpbatoi.controllers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.cipfpbatoi.model.Tarea;
import es.cipfpbatoi.services.MariadbConnection;

@Controller
public class Act06Controller {
	
	@GetMapping("/tarea-default-add3")
	@ResponseBody
	public String insertaTareaPorDefecto(@RequestParam String descripcion, @RequestParam String usuario) {
		StringBuilder response = new StringBuilder();
		String sql = "insert into tareas values (null, ?, ?, ?, ?, ?, ?, null);";
		try (PreparedStatement statement = MariadbConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);){
			Tarea tarea = new Tarea(descripcion, usuario); 
			statement.setString(1, tarea.getUsuario());
			statement.setString(2, tarea.getDescripcion());
			statement.setTimestamp(3, Timestamp.valueOf(tarea.getFechaCreacion()));
			statement.setTimestamp(4, Timestamp.valueOf(tarea.getFechaVencimiento()));
			statement.setString(5, tarea.getPrioridad().toString());
			statement.setBoolean(6, tarea.isRealizada());			
			//statement.setNull(8, Types.INTEGER);
			
//			System.out.println(statement.toString());
			int filasAfectadas = statement.executeUpdate();
			
			ResultSet rs = statement.getGeneratedKeys();
			rs.next();
			int codigoAsignado = rs.getInt(1);			
			
			response.append("<p style='color:blue'>Nueva tarea con <strong>código " + codigoAsignado + "</strong>: insertada correctamente​</p>");
			response.append("Filas afectadas: " + filasAfectadas);
			
			return response.toString();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return "<h1> Algo ha ido mal... </h1>";
		}		
	}
}
