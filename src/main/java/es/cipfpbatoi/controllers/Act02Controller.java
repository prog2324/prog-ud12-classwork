package es.cipfpbatoi.controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.cipfpbatoi.services.MariadbConnection;

@Controller
public class Act02Controller {
	
	@GetMapping("/actividad2")
	@ResponseBody
	public String actividad2() {
		StringBuilder response = new StringBuilder();
		
		try (Statement statement = MariadbConnection.getConnection().createStatement();){
			String sql = "select codigo, descripcion from tareas";
			ResultSet rs = statement.executeQuery(sql);
			response.append("<p style='color:blue'><strong>Primera consulta</strong>: Mostrar por pantalla el código y la descripción de todas las tareas.​</p>");
			while (rs.next()) {				
				response.append(rs.getInt(1) + " - " + rs.getString(2) + "<br>");
			}			
			
			sql = "select nombre from categorias";
			rs = statement.executeQuery(sql);
			response.append("<p style='color:blue'><strong>Segunda consulta</strong>: Mostrar por pantalla todos los nombres de las categorías​</p>");
			while (rs.next()) {				
				response.append(rs.getString(1) + "<br>");
			}	
			
			sql = "select descripcion, fechaCreacion, realizada "
					+ "from tareas where categoria_id in (1,2);";
			rs = statement.executeQuery(sql);
			response.append("<p style='color:blue'><strong>Tercera consulta</strong>: Mostrar por pantalla la descripción, la fecha de creación (en formato dd/mm/yyyy), si ha sido realizada o no (mostrando sí o no)  de todas las tareas de categoría 1 o 2.​​</p>");
			while (rs.next()) {		
				String fechaCreacion = rs.getDate(2).toLocalDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
				String realizada = rs.getBoolean(3)? "SI": "NO";
				response.append(rs.getString(1) + " - " + fechaCreacion + " - " + realizada + "<br>");
			}
			
			rs.close();
			return response.toString();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return "<h1> Algo ha ido mal... </h1>";
		}		
	}
}
