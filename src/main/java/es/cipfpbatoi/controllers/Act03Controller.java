package es.cipfpbatoi.controllers;

import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.cipfpbatoi.model.Tarea;
import es.cipfpbatoi.services.MariadbConnection;

@Controller
public class Act03Controller {

	@GetMapping("/tarea-default-add")
	@ResponseBody
	public String insertarTareaPorDefecto(@RequestParam String desc, @RequestParam String usr) {
		StringBuilder response = new StringBuilder();

		try(Statement st = MariadbConnection.getConnection().createStatement()) {
			Tarea tarea = new Tarea(desc, usr);
			String fecCreacion = tarea.getFechaCreacion().toString();
			String fecVencimiento = tarea.getFechaVencimiento().toString();
			String sql = 
					String.format("insert into tareas values (null, '%s', '%s', '%s', '%s', '%s', %b, null)",
				    tarea.getUsuario(), tarea.getDescripcion(), fecCreacion, 
				    fecVencimiento, tarea.getPrioridad(), tarea.isRealizada());
			System.out.println(sql);
			int filasAfectadas = st.executeUpdate(sql);
			response.append("<p style='color:blue'><strong>Inserción de tarea </strong>: realizada correctamente​</p>");
			response.append("Filas afectadas: " + filasAfectadas);
			return response.toString();
			
		} catch (SQLException e) {
			return "<h1> Algo ha ido mal... </h1>";
		}			
		
		
	}
}
