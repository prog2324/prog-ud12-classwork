package es.cipfpbatoi.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MariadbConnection {
	private static Connection connection;
	private final static String IP = "192.168.56.101";
	private final static String DATABASE = "tareas_db";
	private final static String USERNAME = "batoi";
	private final static String PASSWORD = "1234";

	public static Connection getConnection() throws SQLException {
		if (connection == null || connection.isClosed()) {
			String dbURL = "jdbc:mariadb://" + IP + "/" + DATABASE;
			connection = DriverManager.getConnection(dbURL, USERNAME, PASSWORD);
//			System.out.println("Conexión válida: " + connection.isValid(20));
		}
		return connection;
	}

	public static void closeConnection() throws SQLException {
		if (connection != null && !connection.isClosed()) {
			connection.close();
			connection = null;
		}
	}

}