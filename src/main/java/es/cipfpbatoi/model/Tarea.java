package es.cipfpbatoi.model;

import java.time.LocalDateTime;

public class Tarea {
	
	public enum Prioridad {
		ALTA, MEDIA, BAJA;
	}

	private int codigo;
	private String usuario;
	private String descripcion;
	private LocalDateTime fechaCreacion;
	private LocalDateTime fechaVencimiento;
	private Prioridad prioridad;
	private boolean realizada;
	private int categoriaId;

	public Tarea(String descripcion, String usuario) {
		this.codigo = 0; // no se establece
		this.descripcion = descripcion;
		this.usuario = usuario;
		this.fechaCreacion = LocalDateTime.now();
		this.fechaVencimiento = this.fechaCreacion.plusDays(2);
		this.prioridad = Prioridad.BAJA;
		this.realizada = false;
//		this.categoriaId = 0; // sin categoria
	}

	public int getCodigo() {
		return codigo;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public LocalDateTime getFechaVencimiento() {
		return fechaVencimiento;
	}

	public Prioridad getPrioridad() {
		return prioridad;
	}

	public boolean isRealizada() {
		return realizada;
	}

	public int getCategoriaId() {
		return categoriaId;
	}

}
